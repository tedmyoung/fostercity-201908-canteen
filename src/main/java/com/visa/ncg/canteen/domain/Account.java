package com.visa.ncg.canteen.domain;

public class Account {
  private Long id;
  private int balance;
  private String name;

  public Account(int balance, String name) {
    this.balance = balance;
    this.name = name;
  }

  public Account() {
  }

  public String name() {
    return name;
  }

  public void changeNameTo(String newName) {
    name = newName;
  }

  public int balance() {
    return balance;
  }

  public void deposit(int amount) {
    validateAmount(amount);
    balance += amount;
  }

  public void withdraw(int amount) {
    validateAmount(amount);
    validateSufficientBalance(amount);
    balance -= amount;
  }

  private void validateSufficientBalance(int amount) {
    if (balance < amount) {
      throw new InsufficientBalanceException();
    }
  }

  private void validateAmount(int amount) {
    if (amount <= 0) {
      throw new InvalidAmountException();
    }
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
