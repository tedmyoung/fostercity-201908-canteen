package com.visa.ncg.canteen;

import com.visa.ncg.canteen.domain.Account;

public class AccountResponse {
  private long id;
  private int balance;
  private String name;

  static AccountResponse transformFrom(Account account) {
    AccountResponse accountResponse = new AccountResponse();
    accountResponse.setId(account.getId());
    accountResponse.setName(account.name());
    accountResponse.setBalance(account.balance());
    return accountResponse;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public int getBalance() {
    return balance;
  }

  public void setBalance(int balance) {
    this.balance = balance;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    AccountResponse that = (AccountResponse) o;

    if (id != that.id) return false;
    if (balance != that.balance) return false;
    return name.equals(that.name);
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + balance;
    result = 31 * result + name.hashCode();
    return result;
  }
}