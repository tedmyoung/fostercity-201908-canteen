package com.visa.ncg.canteen;

import com.visa.ncg.canteen.domain.Account;

public class AccountCreateRequest {
  private int initialBalance;
  private String accountName;

  Account toDomain() {
    return new Account(initialBalance, accountName);
  }

  public int getInitialBalance() {
    return initialBalance;
  }

  public void setInitialBalance(int initialBalance) {
    this.initialBalance = initialBalance;
  }

  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    AccountCreateRequest that = (AccountCreateRequest) o;

    if (initialBalance != that.initialBalance) return false;
    return accountName.equals(that.accountName);
  }

  @Override
  public int hashCode() {
    int result = initialBalance;
    result = 31 * result + accountName.hashCode();
    return result;
  }
}
