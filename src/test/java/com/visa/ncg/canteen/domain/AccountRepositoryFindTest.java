package com.visa.ncg.canteen.domain;

import com.visa.ncg.canteen.FakeAccountRepository;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountRepositoryFindTest {

  @Test
  public void finalAllOnNewRepositoryReturnsEmptyList() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();

    assertThat(accountRepository.findAll())
        .isEmpty();
  }

  @Test
  public void findAllShouldReturn2Accounts() {
    Account account1 = new Account();
    account1.setId(1L);
    Account account2 = new Account();
    account2.setId(2L);


    AccountRepository repo = new FakeAccountRepository();
    repo.save(account1);
    repo.save(account2);
    assertThat(repo.findAll())
        .hasSize(2);
  }

  @Test
  public void findAllShouldReturnPredefinedAccount() throws Exception {
    Account account = new Account();
    account.setId(3L);

    AccountRepository repo = new FakeAccountRepository();
    repo.save(account);
    assertThat(repo.findAll().get(0).getId())
        .isEqualTo(3L);
  }

  @Test
  public void findOneForNonExistentIdReturnsNull() throws Exception {
    AccountRepository repo = new FakeAccountRepository();

    assertThat(repo.findOne(7L))
        .isNull();
  }

  @Test
  public void findOneForExistingIdReturnsThatAccount() throws Exception {
    Account account = new Account();
    account.setId(5L);
    AccountRepository repo = new FakeAccountRepository();
    repo.save(account);

    assertThat(repo.findOne(5L).getId())
        .isEqualTo(5L);
  }

}
