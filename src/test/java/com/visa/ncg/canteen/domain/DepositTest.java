package com.visa.ncg.canteen.domain;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class DepositTest {

  @Test
  public void newAccountHasZeroBalance() throws Exception {
    Account account = new Account();

    assertThat(account.balance())
        .isZero();
  }

  @Test
  public void deposit99IntoNewAccountResultsInBalanceOf99() throws Exception {
    Account account = new Account();

    account.deposit(99);

    assertThat(account.balance())
        .isEqualTo(99);
  }

  @Test
  public void deposit23AndDeposit32ResultsInBalanceOf55() throws Exception {
    Account account = new Account();

    account.deposit(23);
    account.deposit(32);

    assertThat(account.balance())
        .isEqualTo(55);
  }

  @Test
  public void deposit0ThrowsInvalidAmountException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.deposit(0);
    })
        .isInstanceOf(InvalidAmountException.class);
  }

  @Test
  public void depositNegative1ThrowsInvalidAmountException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.deposit(-1);
    })
        .isInstanceOf(InvalidAmountException.class);
  }

}
