package com.visa.ncg.canteen.domain;

import com.visa.ncg.canteen.FakeAccountRepository;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountRepositorySaveTest {

  @Test
  public void savedAccountCanBeFoundByFindOne() throws Exception {
    AccountRepository repo = new FakeAccountRepository();
    Account account = new Account();
    account.setId(9L);

    repo.save(account);

    assertThat(repo.findOne(9L).getId())
        .isEqualTo(9L);
  }

  @Test
  public void newlySavedAccountIsAssignedAnId() throws Exception {
    AccountRepository repo = new FakeAccountRepository();
    Account account = new Account();

    Account savedAccount = repo.save(account);

    assertThat(savedAccount.getId())
        .isNotNull();
  }

  @Test
  public void saveAccountWithIdHasChangedAttributesWhenFound() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();
    Account account = new Account();
    account.deposit(15);
    account.setId(12L);
    accountRepository.save(account);

    // Create a "copy" of the account, same ID, different balance
    Account account12 = new Account();
    account12.deposit(8);
    account12.setId(12L);
    accountRepository.save(account12);

    // Then it should have the new balance when we find it in the repository
    Account found = accountRepository.findOne(12L);
    assertThat(found.balance())
        .isEqualTo(8);
  }
}
