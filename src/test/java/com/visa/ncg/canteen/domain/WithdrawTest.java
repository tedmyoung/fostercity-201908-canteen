package com.visa.ncg.canteen.domain;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class WithdrawTest {

  @Test
  public void withdraw7FromAccountWith11BalanceIs4() throws Exception {
    Account account = new Account();
    account.deposit(11);

    account.withdraw(7);

    assertThat(account.balance())
        .isEqualTo(4);
  }

  @Test
  public void withdraw6Then8FromAccountWith22BalanceIs8() throws Exception {
    Account account = new Account();
    account.deposit(22);

    account.withdraw(6);
    account.withdraw(8);

    assertThat(account.balance())
        .isEqualTo(8);
  }

  @Test
  public void withdrawZeroThrowsInvalidException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.withdraw(0);
    })
        .isInstanceOf(InvalidAmountException.class);
  }

  @Test
  public void withdrawNegative1ThrowsInvalidException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.withdraw(-1);
    })
        .isInstanceOf(InvalidAmountException.class);
  }

  @Test
  public void withdraw17FromAccountWith16ThrowsException() throws Exception {
    Account account = new Account();
    account.deposit(16);

    assertThatThrownBy(() -> {
      account.withdraw(17);
    })
        .isInstanceOf(InsufficientBalanceException.class);
  }
}
