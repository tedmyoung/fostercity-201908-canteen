package com.visa.ncg.canteen;

import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.AccountRepository;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountApiControllerTest {

  @Test
  public void accountInfoReturnsAccountResponseWithSpecifiedId() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();
    Account account = new Account();
    account.deposit(73);
    account.setId(123L);
    accountRepository.save(account);

    AccountApiController controller = new AccountApiController(accountRepository);

    AccountResponse accountResponse = controller.accountInfo("123");

    assertThat(accountResponse.getBalance())
        .isEqualTo(73);
  }

}