package com.visa.ncg.canteen;

import com.visa.ncg.canteen.domain.Account;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountCreateRequestTest {

  @Test
  public void accountCreatedFromAllPropertiesInRequest() throws Exception {
    AccountCreateRequest accountCreateRequest = new AccountCreateRequest();
    accountCreateRequest.setAccountName("Lunch");
    accountCreateRequest.setInitialBalance(80);

    Account account = accountCreateRequest.toDomain();

    assertThat(account.getId())
        .isNull();

    assertThat(account.balance())
        .isEqualTo(80);

    assertThat(account.name())
        .isEqualTo("Lunch");
  }

}