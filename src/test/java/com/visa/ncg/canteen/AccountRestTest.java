package com.visa.ncg.canteen;

import com.visa.ncg.canteen.domain.AccountRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AccountRestTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private AccountRepository accountRepository;

  @Test
  public void getReturnsJsonContainingBalance() throws Exception {
    MvcResult result = mockMvc.perform(get("/api/accounts/1"))
                              .andExpect(status().isOk())
                              .andReturn();
    String body = result.getResponse().getContentAsString();
    assertThat(body)
        .contains("\"balance\"");
  }

  @Test
  public void getWithAccountIdReturnsAccountAndBalanceForThatId() throws Exception {
    mockMvc.perform(get("/api/accounts/1"))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$.id").value(1))
           .andExpect(jsonPath("$.balance").value("20"));
  }

  @Test
  public void postWithBalanceAndNameCreatesNewAccountInRepository() throws Exception {
    mockMvc.perform(post("/api/accounts")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content("{\"initialBalance\":32,\"accountName\":\"Video Games\"}"))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$.name").value("Video Games"))
           .andExpect(jsonPath("$.balance").value("32"));

    // the repository had 2 accounts (from the AccountDataLoader) and now should have 3
    assertThat(accountRepository.findAll())
        .hasSize(3);
  }
}