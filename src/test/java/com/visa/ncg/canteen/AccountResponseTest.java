package com.visa.ncg.canteen;

import com.visa.ncg.canteen.domain.Account;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountResponseTest {

  @Test
  public void transformFromCopiesAllDataFromAccount() throws Exception {
    Account account = new Account(46, "Games");
    account.setId(8L);

    AccountResponse accountResponse = AccountResponse.transformFrom(account);

    AccountResponse expectedResponse = new AccountResponse();
    expectedResponse.setName("Games");
    expectedResponse.setBalance(46);
    expectedResponse.setId(8L);

    assertThat(accountResponse)
        .isEqualTo(expectedResponse); // requires equals() to be implemented in AccountResponse
  }
}